<?php
/**
 * Settings for Puntuarte Widgets.
 *
 * @file: commerce_puntuarte.admin.inc
 */
function commerce_puntuarte_settings_form($form_state) {
  $form['commerce_puntuarte_api_key'] = array(
    '#type' => 'textfield',
    '#title' => t('API Key'),
    '#default_value' => variable_get('commerce_puntuarte_api_key', ''),
    '#description' => t('The Puntuarte API Key'),
    '#required' => TRUE,
  );

  $node_fields = field_info_instances('node');
  $product_field_options = array();
  foreach ($node_fields as $field_instances) {
    foreach ($field_instances as $field) {
      $field_info = field_info_field($field['field_name']);
      if ($field_info['type'] == 'commerce_product_reference' || $field_info['type'] == 'entityreference') {
        $product_field_options[$field['field_name']] = $field['label'] . ' (' . $field['field_name'] . ')';
      }
    }
  }
  $form['commerce_puntuarte_product_field'] = array(
    '#type' => 'select',
    '#title' => t('Product field'),
    '#description' => t('Select the field that will be used as the product on nodes for Puntuarte.'),
    '#default_value' => variable_get('commerce_puntuarte_product_field', 'field_product'),
    '#options' => $product_field_options,
  );

  $product_fields = field_info_instances('commerce_product');
  $form = system_settings_form($form);
  $form['#submit'][] = 'commerce_puntuarte_settings_form_submit';

  return $form;
}

/**
 * Implements hook_form_submit().
 */
function commerce_puntuarte_settings_form_submit($form, &$form_state) {
  $api_key = $form_state['values']['commerce_puntuarte_api_key'];
  $response = drupal_http_request(
    'https://www.puntuarte.com/API/verify/api_key/' . $api_key,
      array(
        'method' => 'POST',
      )
  );
  if (isset($response->code) && $response->code == 200) {
    variable_set('commerce_puntuarte_api_key', $api_key);
  }
  else {
    form_set_error('form', t('There was an error with the API KEY validation service'));
    $form_state['redirect'] = FALSE;
  }
}
