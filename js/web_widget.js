/**
 * @file
 * Bridge to retrieve data from Puntuarte API.
 */

(function() {
    var jQuery;
    /* Load jQuery if not present */
    if (window.jQuery === undefined || window.jQuery.fn.jquery !== '1.4.2') {
        var script_tag = document.createElement('script');
        script_tag.setAttribute("type","text/javascript");
        script_tag.setAttribute(
            "src",
            "http://ajax.googleapis.com/ajax/libs/jquery/1.4.2/jquery.min.js"
        );
        if (script_tag.readyState) {
            /*For old versions of IE!*/
            script_tag.onreadystatechange = function () {
                if (this.readyState == 'complete' || this.readyState == 'loaded') {
                    scriptLoadHandler();
                }
            };
        }
        else {
            script_tag.onload = scriptLoadHandler;
        }
        /* Try to find the head, otherwise default to the documentElement */
        (document.getElementsByTagName("head")[0] || document.documentElement).appendChild(script_tag);
    }
    else {
        /* The jQuery version on the window is the one we want to use */
        jQuery = window.jQuery;
        main();
    }

    /* Called once jQuery has loaded */
    function scriptLoadHandler() {
        /* Restore $ and window.jQuery to their previous values and store the new jQuery in our local jQuery variable*/
        jQuery = window.jQuery.noConflict(true);
        main();
    }

    /* Main function */
    function main() {
        jQuery(document).ready(
            function($) {
                /* Load CSS */
                var script = document.getElementById('puntuarte-widget-embedder').previousSibling;
                /* We want the script we have inserted before our script tag builder */
                var data_appkey = $(script).attr('data-appkey');
                /* Data-appkey not set */
                if ((data_appkey !== '' && data_appkey !== undefined) || data_appkey.length > 0) {
                    var css_link = $(
                        "<link>", {
                            rel: "stylesheet",
                            type: "text/css",
                            href: "style.css"
                        }
                    );
                    css_link.appendTo('head');
                    /* Load HTML for Widget 1 */
                    var jsonp_url_site = "http://puntuarte.com/avg/ratings/" + data_appkey;
                    $.ajax(
                        {
                            url: jsonp_url_site,
                            async: true,
                            type: "GET",
                            dataType: "json",
                            success: function (result) {
                                if (result !== null) {
                                    $('#puntuarte-global-widget').addClass('active');
                                    $('#puntuarte-global-widget').html('<div class="header-rating"><span class="stars" data-rating="' + result.ratings_avg + '"></span></div><div class="body-rating"><div class="ratings-average"><strong>' + result.ratings_avg + '/5</strong> Media</div><div class="ratings-total"><strong>' + result.ratings_count + '</strong> Valoraciones</div></div><div class="ratings-footer">Valoraciones ofrecidas por <br/> <a href="#">Puntuarte.com</a></div></div>');
                                    $(".stars").each(
                                        function() {
                                            /* Get the value */
                                            var val = $(this).attr("data-rating");
                                            val = Math.round(val * 2) / 2; /* To round to nearest half */
                                            /* Make sure that the value is in 0 - 5 range, multiply to get width */
                                            var size = Math.max(0, (Math.min(5, val))) * 16;
                                            /* Create stars holder */
                                            var $span = $('<span />').width(size);
                                            /* Replace the numerical value with stars */
                                            $(this).html($span);
                                        }
                                    );
                                }
                            },
                            error: function (xhr, ajaxOptions, thrownError) {
                                console.log(xhr);
                            }
                        }
                    );
                    /* Load HTML for Widget 2 */
                    $(".puntuarte-product-widget").each(
                        function(){
                            var product_sku = $(this).attr("data-product-sku");
                            var product_object = $(this);
                            if (product_sku !== null || product_sku !== '') {
                                var jsonp_url_product = "http://puntuarte.com/avg/product-ratings/" + data_appkey + "/" + product_sku;
                                $.ajax(
                                    {
                                        url: jsonp_url_product,
                                        async: false,
                                        type: "GET",
                                        dataType: "json",
                                        success: function (result) {
                                            if (result !== null) {
                                                var user_review = {};
                                                var row = '';
                                                $.each(
                                                    result[1], function(index, value){
                                                        user_review = $(this).get(0);
                                                        row = row + '<div class="row"><div class="name">' + user_review.reviewer_name + '</div><span class="stars" data-rating="' + user_review.count + '"></span><div class="comment">' + user_review.comment + '</div></div>';
                                                    }
                                                );
                                                $(product_object).addClass('active');
                                                $(product_object).html('<div class="header-rating"><div class="ratings-total"><div class="puntuarte-widget-wrapper-stars"><span class="stars" data-rating="' + result[0].ratings_avg + '"></span></div><div class="product-widget-avg"><span class="puntuarte-widget-number"><strong>' + result[0].ratings_avg + '/5</strong></span> Media  </div><div class="product-widget-totals"><span class="puntuarte-widget-number"><strong>' + result[0].ratings_count + '</strong></span> Valoraciones</div></div></div><div class="body-rating">' + row + '</div></div>');
                                                $(".stars").each(
                                                    function() {
                                                        /* Get the value */
                                                        var val = $(this).attr("data-rating");
                                                        /* Round to nearest half */
                                                        val = Math.round(val * 2) / 2;
                                                        /* Make sure that the value is in 0 - 5 range, multiply to get width */
                                                        var size = Math.max(0, (Math.min(5, val))) * 16;
                                                        /* Create stars holder */
                                                        var $span = $('<span />').width(size);
                                                        /* Replace the numerical value with stars */
                                                        $(this).html($span);
                                                    }
                                                );
                                            }
                                        },
                                        error: function (xhr, ajaxOptions, thrownError) {
                                            console.log(xhr);
                                        }
                                    }
                                );
                            }
                            else {
                              console.log("Establezca el data-product-sku en su contenedor");
                            }
                        }
                    )
                    /* Load HTML for Widget 3 */
                    $(".puntuarte-product-mini-widget").each(
                        function(){
                            var product_sku = $(this).attr("data-product-sku");
                            var product_object = $(this);
                            if (product_sku !== null || product_sku !== '') {
                                var jsonp_url_product = "http://puntuarte.com/avg/product-ratings/" + data_appkey + "/" + product_sku;
                                $.ajax(
                                    {
                                        url: jsonp_url_product,
                                        async: false,
                                        type: "GET",
                                        dataType: "json",
                                        success: function (result) {
                                            if (result !== null) {
                                                $(product_object).addClass('active');
                                                $(product_object).html('<span class="stars" data-rating="' + result[0].ratings_avg + '"></span><span class="product-widget-totals">(' + result[0].ratings_count + ')</span>');
                                                $(".stars").each(
                                                    function() {
                                                        /* Get the value */
                                                        var val = $(this).attr("data-rating");
                                                        val = Math.round(val * 2) / 2; /* To round to nearest half */
                                                        /* Make sure that the value is in 0 - 5 range, multiply to get width */
                                                        var size = Math.max(0, (Math.min(5, val))) * 16;
                                                        /* Create stars holder */
                                                        var $span = $('<span />').width(size);
                                                        /* Replace the numerical value with stars */
                                                        $(this).html($span);
                                                    }
                                                );
                                            }
                                        },
                                        error: function (xhr, ajaxOptions, thrownError) {
                                            console.log(xhr);
                                        }
                                    }
                                );
                            }
                            else {
                              console.log("Establezca el data-product-sku en su contenedor");
                            }
                        }
                    )
                }
                else {
                    console.log('No se ha establecido data-appkey');
                }
            }
        );
    }
})();
