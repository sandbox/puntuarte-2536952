/**
 * @file
 * Drupal behaviors for Commerce Puntuarte.
 */

(function ($) {
    Drupal.behaviors.commerce_puntuarte = {
        attach: function (context, settings) {
            function async_load()
            {
                var s = document.createElement('script');
                s.type = 'text/javascript';
                s.setAttribute('data-appkey', Drupal.settings.commerce_puntuarte.api_key);
                s.async = true;
                var theUrl = 'https://s3-eu-west-1.amazonaws.com/puntuarte.com/widgets/web_widget.js';
                s.src = theUrl + (theUrl.indexOf('?') >= 0 ? '&' : '?') + 'ref=' + encodeURIComponent(window.location.href);
                var embedder = document.getElementById('puntuarte-widget-embedder');
                embedder.parentNode.insertBefore(s, embedder);
            }
            if (window.attachEvent) {
              window.attachEvent('onload', async_load);
            }
            else {
              window.addEventListener('load', async_load, false);
            }
        }
    };
}(jQuery));
