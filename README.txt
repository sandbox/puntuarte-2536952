CONTENTS OF THIS FILE
---------------------
  
* Introduction
* Requirements
* Installation
* Configuration
* FAQ

INTRODUCTION
------------

This plugin integrates your Drupal Commerce site with Puntuarte Reviews system. 

It will allow you to display different widgets at your website, generate customer feedback, drive traffic and increase conversions.

Besides displaying the customer ratings and reviews from products, this plugin sends automatically an email to the customer once a purchase has been completed. This email will allow the customer to give your site feedback letting him to rate his experience and leave a comment that automatically will be added to your widgets, so that information can be useful for other potential customers.

* For a full description of the module, visit the project page:
   https://drupal.org/project/commerce_puntuarte

* To submit bug reports and feature suggestions, or to track changes:
   https://drupal.org/project/issues/commerce_puntuarte


REQUIREMENTS
------------
This module requires the following modules:

* Drupal Commerce (https://www.drupal.org/project/commerce)
* Commerce Order (goes with Drupal Commerce)
* Commerce Product (goes with Drupal Commerce)

INSTALLATION
------------

* Install as you would normally install a contributed Drupal module. See:
   https://drupal.org/documentation/install/modules-themes/modules-7
   for further information.
* You will need to enable the modules required that are mentioned above


CONFIGURATION
-------------

* Configure user permissions in Administration » People » Permissions

* In Modules/Commerce Puntuarte click in "Configure" and enter you API KEY (signup or login in Puntuarte.com to get it).

* Select the product field you want to associate to your Products.

* In Admin/Structure/Blocks three blocks will be generated:

    Puntuarte product reviews: Full format (it's displayed in Product page with the user reviews associated to Product)
    Puntuarte product reviews: Short format (displayed as stars and number of ratings for product. Clicking it you'll get the full format as popup)
    Puntuarte total reviews (total reviews from site and average rating)

* Consider the product widgets blocks must be located at product pages, while de total reviews widget could be located anywhere on your criteria


TROUBLESHOOTING
---------------

* If the widgets does not display, check the following:

   - Are the "Administer Puntuarte" permission enabled for the appropiated roles?


FAQ
---

Q: Why the widgets are not shown?

A: You need a valid API KEY to connect with Puntuarte and get all your associated info from your account. You will find this API KEY in the profile page at your Puntuarte Profile Page(https://www.puntuarte.com/profile). When you set the API KEY correctly, block will show the data associated.

Q: I want to display a specific product widget in other region

A: In order to locate one of the product widgets in a custome location you have to embed the code as it follows:

   Product short format: <div class="puntuarte-product-mini-widget active" data-product-sku="your_product_sku"></div>

   Product full format:  <div class="puntuarte-product-widget active" data-product-sku="your_product_sku"></div>